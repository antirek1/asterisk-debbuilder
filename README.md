# What is it?

This is a shell script to build **unofficial** Debian Stretch packages of the current Asterisk 16 including PJSIP through Gitlab CI.

# Usage

[Artifacts](https://gitlab.com/xa0s/asterisk-debbuilder/-/jobs/290027665/artifacts/download)