#!/bin/bash

set -x -e

asterisk_url="http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-$ASTVERSION.tar.gz"

mountdir="/output"
builddir="/usr/src/build"
contribdir="/usr/src/contrib"
outdir="/usr/src/packages"

# Cleanup build directory
find ${builddir} -mindepth 1 -delete
find ${outdir} -mindepth 1 -delete

### ASTERISK

# Download
sudo -u build curl "${asterisk_url}" | sudo -u build tar xz -C "${builddir}" 

astdir="$(basename $(find "${builddir}" -maxdepth 1 -mindepth 1 -type d -name 'asterisk*'))"
astversion="$(echo ${astdir} | awk -F '-' '{print $2;}')"

# Build

cd ${builddir}/${astdir}
sudo -u build ./configure CFLAGS=-mtune=generic --host=x86_64-linux-gnu --build=x86_64-linux-gnu --with-pjproject-bundled --with-jansson-bundled
sudo -u build contrib/scripts/get_mp3_source.sh
sudo -u build sed -i '/NATIVE_ARCH=/c \NATIVE_ARCH=0' build_tools/menuselect-deps
sudo -u build make menuselect.makeopts
sudo -u build menuselect/menuselect --enable DONT_OPTIMIZE menuselect.makeopts
sudo -u build menuselect/menuselect --enable BETTER_BACKTRACES menuselect.makeopts
sudo -u build menuselect/menuselect --enable app_cdr --enable cdr_odbc --enable func_odbc --enable res_odbc --enable app_macro menuselect.makeopts
sudo -u build menuselect/menuselect --enable pbx_lua --enable hep_pjsip --enable res_snmp --enable res_endpoint_stats --enable res_chan_stats --enable app_statsd --enable res_statsd --enable res_chan_stats --enable res_endpoint_stats menuselect.makeopts
sudo -u build menuselect/menuselect --disable MOH-OPSOUND-WAV --disable CORE-SOUNDS-EN-GSM --enable CORE-SOUNDS-RU-GSM menuselect.makeopts
sudo -u build make

# Create Packages

sudo -u build mkdir -p ${builddir}/root/asterisk
# This will download sounds
sudo -u build make install DESTDIR=${builddir}/root/asterisk
sudo -u build make samples DESTDIR=${builddir}/root/asterisk
sudo -u build mkdir -p ${builddir}/root/asterisk/lib/systemd/system
sudo -u build cp ${contribdir}/asterisk.service ${builddir}/root/asterisk/lib/systemd/system/

sudo -u build fpm -s dir -t deb -n asterisk \
	-v "${astversion}" --epoch 10 \
	-C ${builddir}/root/asterisk \
	-p ${outdir}/asterisk_VERSION_ARCH.deb \
	--category comm \
	--license "GPL2" \
	--description "Open Source Private Branch Exchange (PBX)" \
	--url "http://www.asterisk.org/" \
	-d "systemd" \
	-d "adduser" \
	-d "libsrtp0" \
	-d "libspeex1" \
	-d "libspeexdsp1" \
	-d "libxml2" \
	-d "libxslt1.1" \
	-d "libncurses5" \
	-d "libcurl3" \
	-d "libsqlite3-0" \
	-d "libuuid1" \
	-d "libjansson4" \
	-d "libiksemel-dev" \
	-d "libsnmp-dev" \
	--deb-suggests "asterisk-dev" \
	--after-install ${contribdir}/asterisk-postinst.sh \
	--after-remove ${contribdir}/asterisk-postrm.sh \
	--before-remove ${contribdir}/asterisk-prerm.sh \
	--config-files /etc/asterisk \
	-x usr/include \
	-x usr/sbin/safe_asterisk \
	-x /usr/share/man/man8/safe_asterisk \
	etc \
	lib \
	usr \
	var

sudo -u build fpm -s dir -t deb -n asterisk-dev \
	-v "${astversion}" --epoch 10 \
	-C ${builddir}/root/asterisk \
	-p ${outdir}/asterisk-dev_VERSION_ARCH.deb \
	--description "Development files for Asterisk" \
	--category devel \
	--license "GPL2" \
	--url "http://www.asterisk.org/" \
	-d "asterisk = 10:${astversion}" \
	usr/include

### Finish

# Cleanup build directory
find ${builddir} -mindepth 1 -delete
