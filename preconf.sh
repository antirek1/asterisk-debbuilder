#!/bin/bash
apt-get update && apt-get dist-upgrade -y
apt-get -y install build-essential pkg-config sudo curl ruby ruby-dev \
    binutils-dev libsrtp0-dev libssl-dev libspeex-dev libspeexdsp-dev \
    libgsm1-dev subversion flex libxml2-dev libxslt1-dev libncurses5-dev \
    libcurl4-openssl-dev libsqlite3-dev uuid-dev libjansson-dev \
    libboost-iostreams1.62.0 libclass-accessor-perl libcwidget3v5 \
    libio-string-perl libparse-debianchangelog-perl libsigc++-2.0-0v5 \
    libsub-name-perl libxapian30 unixodbc unixodbc-dev libiksemel-dev \
    libsnmp-dev libedit-dev liblua5.1-0-dev && apt-get clean
gem install fpm
adduser --system --home /usr/src build
mkdir -p /usr/src/build /usr/src/packages
chown -R build: /usr/src
cp -R contrib /usr/src/contrib